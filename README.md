# Telegram-GPT

![Python](https://img.shields.io/badge/-Python-3776AB?logo=python&logoColor=white&color=green)
![OpenAI](https://img.shields.io/badge/-OpenAI-FF6600?logo=openai&logoColor=white)
![Telebot](https://img.shields.io/badge/-telebot-0088CC?logo=telegram&logoColor=white)
![python-dotenv](https://img.shields.io/badge/-python--dotenv-FFE873?logo=python&logoColor=white)

Este projeto é um bot do Telegram integrado à API do GPT da OpenAI para gerar respostas automáticas baseadas em mensagens de texto enviadas pelo usuário.

## Clone o projeto

Para clonar o projeto, execute o seguinte comando em um terminal:

```
git clone https://gitlab.com/gustavoilhamorais/telegram-gpt.git
```

## Dependências

As seguintes dependências devem ser instaladas usando o pip:

- telebot
- openai
- python-dotenv

Você pode instalá-las através do arquivo `requirements.txt` usando o seguinte comando:

```
pip install -r requirements.txt
```

## Variáveis de ambiente

O bot do Telegram requer algumas variáveis de ambiente para funcionar corretamente. É necessário criar um arquivo `.env` com as seguintes variáveis:

```
TELEGRAM_BOT_TOKEN=""
OPENAI_TOKEN=""
GPT_MAX_TOKENS=256
TELEGRAM_ALLOWED_USER_ID=""
GPT_MODEL_ENGINE="text-davinci-002"
```

O arquivo `.env.example` pode ser utilizado como boilerplate para a criação do `.env`.

A variável `TELEGRAM_ALLOWED_USER_ID` deve conter o ID da conta de usuário do Telegram para a qual o bot tem permissão de responder. Isso significa que o bot só irá acionar a integração com a OpenAI quando receber mensagens desse ID específico.

As variáveis que começam com `GPT` já estão preenchidas, mas podem ser modificadas de acordo com a preferência do usuário.

## Como executar o programa

Para rodar o programa, basta executar o seguinte comando na pasta raiz do projeto:

```
python main.py
```

## Comandos disponíveis

O bot suporta os seguintes comandos:

| Comando                                  | Descrição                                                                                          |
| ---------------------------------------- | -------------------------------------------------------------------------------------------------- |
| /start                                   | Instrui o usuário sobre como utilizar o bot.                                                       |
| /help                                    | Instrui o usuário sobre como utilizar o bot.                                                       |
| /ask \<texto a ser processado pelo GPT\> | Envia o texto para ser processado pelo modelo de IA e retorna uma resposta gerada automaticamente. |

## Licença

Este projeto está licenciado sob a [Mozilla Public License Version 2.0 (MPL-2.0)](https://opensource.org/licenses/MPL-2.0). Para obter mais informações, consulte o arquivo LICENSE.

## Contribuições

Todas as contribuições são bem-vindas, mas serão avaliadas de acordo com a disponibilidade de tempo do mantenedor do projeto. As contribuições serão aceitas se consideradas válidas pelo mantenedor do projeto.
