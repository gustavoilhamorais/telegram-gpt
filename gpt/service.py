import os
import openai
from dotenv import load_dotenv

load_dotenv()

GPT_MODEL_ENGINE = os.getenv("GPT_MODEL_ENGINE")
GPT_MAX_TOKENS = int(os.getenv("GPT_MAX_TOKENS"))
openai.api_key = os.getenv("OPENAI_TOKEN")


def format_response(response):
    return response.choices[0].text.strip()


def ask(question):
    return format_response(
        openai.Completion.create(
            engine=GPT_MODEL_ENGINE, prompt=question, max_tokens=GPT_MAX_TOKENS)
    )
