import telebot
import os
from dotenv import load_dotenv
from gpt.service import ask

load_dotenv()

TELEGRAM_TOKEN = os.getenv("TELEGRAM_BOT_TOKEN")
ALLOWED_USER = int(os.getenv("TELEGRAM_ALLOWED_USER_ID"))

bot = telebot.TeleBot(TELEGRAM_TOKEN, parse_mode=None)


@bot.message_handler(func=lambda message: message.from_user.id != ALLOWED_USER)
def handle_user_not_allowed(message):
    bot.reply_to(message, "Você não possui permissões suficientes.")


@bot.message_handler(commands=["start", "help"])
def handle_start_help(message):
    bot.reply_to(message, ("Utilize o comando /ask para fazer perguntas.\n"
                           "Ex.: /ask Que gênero musical a banda ACDC se enquadra?")
                 )


@bot.message_handler(commands=["ask"])
def handle_ask_gpt(message):
    if message.content_type == "text":
        question = message.text.split("/ask ")
        answer = ask(question[1]) if len(
            question) == 2 else "Faça uma pergunta válida!"
        bot.reply_to(message, answer)
    else:
        bot.reply_to(message, "No momento, consigo responder somente textos.")


def start_service():
    bot.infinity_polling()
